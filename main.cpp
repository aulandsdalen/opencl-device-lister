#include <iostream>
#include "lib/cl.hpp" 
#include <vector>
#include "lib/main.hpp"

int main(int argc, char** argv) {
	std::string clInfoFilename = "";
	if (argc > 1) {
		for (int i = 1; i < argc; ++i) {
			if (returnInfoByCommandLineArgument(parseCommandLineArguments(argv[i])).length() > 0)
				std::cout<<returnInfoByCommandLineArgument(parseCommandLineArguments(argv[i]))<<std::endl;
			else 
				clInfoFilename = argv[i];
		}
	}
	std::vector<cl::Platform> all_platforms;  
	std::vector<cl::Device> all_devices;
	cl::Platform::get(&all_platforms);
	if(all_platforms.size() == 0) {
		std::cerr<<"No OpenCL platforms available, perhaps you should check GPU driver installation"<<std::endl;
		exit(1);
	}	
	cl::Platform default_platform = all_platforms[0];
	default_platform.getDevices(CL_DEVICE_TYPE_ALL, &all_devices);
	for (int i = 0; i < all_devices.size(); ++i) {
		std::cout<<"Device: "<<all_devices[i].getInfo<CL_DEVICE_NAME>()<<" supports "<<all_devices[i].getInfo<CL_DEVICE_VERSION>()<<std::endl;
	}
	writeClInfo(clInfoFilename, all_devices);
	return 0;
}
