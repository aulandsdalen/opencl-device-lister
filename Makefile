CXX=clang++
CXXFLAGS+=-std=c++03 -O2 -Wall -Wno-deprecated-declarations
SOURCES+=*.cpp
LIBS+=-framework OpenCL
all:
	$(CXX) $(CXXFLAGS) $(LIBS) $(SOURCES) -o bin/clInfo 
