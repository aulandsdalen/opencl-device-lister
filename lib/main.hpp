#include <fstream> 
#include <string>
#define PROGRAMVERSION "0.2"
enum {ARG_HELP = 10, ARG_INFO = 20, ARG_VERSION = 30, ARG_UNKNOWN = 42};
int writeClInfo(std::string filename, std::vector<cl::Device> &clDevices) {
	std::ofstream clInfoFile;
	if(filename.length() == 0) 
		filename = "clInfo.txt";
	clInfoFile.open(filename);
	clInfoFile<<clDevices.size()<<" devices in total"<<std::endl;
	for (int i = 0; i < clDevices.size(); ++i) 
		clInfoFile<<clDevices[i].getInfo<CL_DEVICE_NAME>()<<" supports "<<clDevices[i].getInfo<CL_DEVICE_VERSION>()<<std::endl;
	clInfoFile.close();
	return 0;
}

int parseCommandLineArguments(char* argument) {
	std::string args = std::string(argument);
			if (args == "help")
				return ARG_HELP;
			else if (args == "info")
				return ARG_INFO;
			else if (args == "version")
				return ARG_VERSION;
			else
				return ARG_UNKNOWN;
}

std::string returnInfoByCommandLineArgument(int enumeratedValue) {
	if (enumeratedValue == ARG_HELP)
		return "help text goes here";
	else if (enumeratedValue == ARG_INFO)
		return "clInfo -- get OpenCL devices information";
	else if (enumeratedValue == ARG_VERSION)
		return PROGRAMVERSION;
	else 
		return ""; 
}

